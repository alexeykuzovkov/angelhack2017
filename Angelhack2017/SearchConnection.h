//
//  SearchConnection.h
//  avito
//
//  Created by Alex Kuzovkov on 06.09.16.
//  Copyright © 2016 Alex Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SearchConnection;
@protocol SearchConnectionDelegate <NSObject>
-(void)searchConnectionFinishedLoadingData:(NSDictionary*)loadedData;
-(void)searchConnectionErrorWithText:(NSString*)error;
@end

@interface SearchConnection : NSObject
@property (nonatomic, weak) id <SearchConnectionDelegate> delegate;
@property (nonatomic, strong) NSDictionary *loadedData;

//+ (SearchConnection *)sharedInstance;
- (void)connect:(NSString*)url;

-(id)initWithDelegate:(id<SearchConnectionDelegate>)delegate;
@end
