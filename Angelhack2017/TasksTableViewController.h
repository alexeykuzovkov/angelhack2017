//
//  TasksTableViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"
@class TasksTableViewController;
@protocol TaskViewDelegate <NSObject>
-(void)taskViewDidChange;
@end

@interface TasksTableViewController : UITableViewController <SearchConnectionDelegate>
@property (nonatomic, weak) id <TaskViewDelegate> delegate;
@property (nonatomic, strong) NSArray *tasks;
@end
