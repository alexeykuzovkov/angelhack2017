//
//  TaskEditViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"

@interface TaskEditViewController : UIViewController <SearchConnectionDelegate>

@property (nonatomic, strong) NSDictionary *taskData;
@property (nonatomic, strong) NSString *gameId;

@property (nonatomic, strong) IBOutlet UITextField *nameField;
@property (nonatomic, strong) IBOutlet UISegmentedControl *pointsControl;

-(IBAction)save:(id)sender;

@end
