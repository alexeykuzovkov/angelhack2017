//
//  GameViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "GameViewController.h"


@interface GameViewController ()
{
    NSDictionary *gameData;
    FieldViewController *fieldView;
    TasksTableViewController *tasksView;
}
@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [self reloadData];
}
-(void)reloadData {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/get_info_token/%@", _game_token]];
}
-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"loaded data %@",loadedData);
    if([loadedData objectForKey:@"data"]) {
        gameData = [loadedData objectForKey:@"data"];
        [self parseGameData];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

-(void)parseGameData {
    [tasksView setTasks:[gameData objectForKey:@"tasks"]];
    [tasksView setDelegate:self];

    [fieldView.gameFieldView setPrizes:[gameData objectForKey:@"prizes"]];
    [fieldView.gameFieldView setPoints:[[gameData objectForKey:@"personal_points"] intValue]];
    [fieldView.gameFieldView beginPlayerMovement];
}

-(void)taskViewDidChange {
    [self reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"field"]) {
        fieldView = (FieldViewController*)[segue destinationViewController];
    }
    
    if ([segue.identifier isEqualToString:@"tasks"]) {
        tasksView = (TasksTableViewController*)[segue destinationViewController];
    }
}


@end
