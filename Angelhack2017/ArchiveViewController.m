//
//  ArchiveViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ArchiveViewController.h"

#import "TaskTableViewCell.h"

@interface ArchiveViewController ()

@end

@implementation ArchiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TaskCell";
    TaskTableViewCell *cell = (TaskTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"TaskTableViewCell"
                                                              owner:self
                                                            options:nil];
        cell = [arrayCellXib objectAtIndex:0];
    }
    
    
    [cell.taskNameLabel setText:@"Hello, world!"];
    [cell.taskNumber setText:[NSString stringWithFormat:@"%d",(int)indexPath.row+1]];
    
    if(indexPath.row>2) {
        [cell setIsDone:true];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end
