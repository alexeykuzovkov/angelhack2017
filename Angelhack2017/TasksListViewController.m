//
//  TasksListViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "TasksListViewController.h"
#import "TaskTableViewCell.h"
#import "TaskEditViewController.h"
@interface TasksListViewController ()

@end

@implementation TasksListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/get_info/%@", _gameId]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    if([loadedData objectForKey:@"data"]) {
        NSDictionary* data = [loadedData objectForKey:@"data"];
        _tasks = [data objectForKey:@"tasks"];
        [self.tableView reloadData];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TaskTableViewCell";
    TaskTableViewCell *cell = (TaskTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"TaskTableViewCell"
                                                              owner:self
                                                            options:nil];
        cell = [arrayCellXib objectAtIndex:0];
    }
    
    
    [cell.taskNameLabel setText:[[_tasks objectAtIndex:indexPath.row] objectForKey:@"text"]];
    [cell.taskNumber setText:[NSString stringWithFormat:@"%d",(int)indexPath.row+1]];
    
    NSNumber *finished = (NSNumber*)[[_tasks objectAtIndex:indexPath.row] objectForKey:@"finished"];
    [cell setIsDone:[finished isEqual:[NSNumber numberWithInt:0]]?false:true];


//    [cell.imageView setImage:[UIImage imageNamed:@"planet_2.png"]];
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tasks.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self performSegueWithIdentifier:@"edit_task" sender:[_tasks objectAtIndex:indexPath.row]];
}

-(IBAction)add:(id)sender {
    [self performSegueWithIdentifier:@"edit_task" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"edit_task"]) {
        TaskEditViewController *e = (TaskEditViewController*)[segue destinationViewController];
        e.gameId = _gameId;
        if (sender!=nil) {
            e.taskData = (NSDictionary*)sender;
        }
    }
}


@end
