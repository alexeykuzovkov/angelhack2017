//
//  PrizesListViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "PrizesListViewController.h"
#import "ImageTableViewCell.h"
#import "PriceEditorViewController.h"
@interface PrizesListViewController ()

@end

@implementation PrizesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/get_info/%@", _gameId]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    if([loadedData objectForKey:@"data"]) {
        NSDictionary* data = [loadedData objectForKey:@"data"];
        _prizes = [data objectForKey:@"prizes"];
        [self.tableView reloadData];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}


-(IBAction)add:(id)sender {
    [self performSegueWithIdentifier:@"edit_prize" sender:nil];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"edit_prize"]) {
        PriceEditorViewController *e = (PriceEditorViewController*)[segue destinationViewController];
        e.gameId = _gameId;
        if (sender!=nil) {
            e.prizeData = (NSDictionary*)sender;
        }
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ImageTableViewCell";
    ImageTableViewCell *cell = (ImageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"ImageTableViewCell"
                                                              owner:self
                                                            options:nil];
        cell = [arrayCellXib objectAtIndex:0];
    }
    
    
    [cell.label setText:[[_prizes objectAtIndex:indexPath.row] objectForKey:@"review"]];
    [cell.imageView setImage:[UIImage imageNamed:
                              [NSString stringWithFormat:@"planet_%@.png",[[_prizes objectAtIndex:indexPath.row] objectForKey:@"model_id"]]
                              ]];
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _prizes.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"edit_prize" sender:[_prizes objectAtIndex:indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}
@end
