//
//  TasksListViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"

@interface TasksListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SearchConnectionDelegate>
@property (nonatomic, strong) NSArray *tasks;
@property (nonatomic, strong) NSString *gameId;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

-(IBAction)add:(id)sender;
@end
