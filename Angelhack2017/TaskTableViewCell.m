//
//  TaskTableViewCell.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "TaskTableViewCell.h"

@implementation TaskTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsDone:(BOOL)isDone {
    _isDone = isDone;
    if (isDone) {
        [_taskNumberBG setImage:[UIImage imageNamed:@"noactivetask.png"]];
        [_taskNumber setTextColor:[UIColor colorWithRed:82/255. green:75/255. blue:91/255. alpha:1]];
    }
    else {
        [_taskNumberBG setImage:[UIImage imageNamed:@"active_task.png"]];
        [_taskNumber setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
    }
}
@end
