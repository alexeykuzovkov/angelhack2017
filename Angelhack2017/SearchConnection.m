//
//  SearchConnection.m
//  avito
//
//  Created by Alex Kuzovkov on 06.09.16.
//  Copyright © 2016 Alex Kuzovkov. All rights reserved.
//

#import "SearchConnection.h"

static NSString *const kBaseURL = @"http://10.18.9.28:3000";

@interface SearchConnection ()
{
    NSURLSessionDataTask *dataTask;
}
@end

@implementation SearchConnection

-(id)init {
    self = [super init];
    if (self) {
        _loadedData = nil;
    }
    return self;
}
-(id)initWithDelegate:(id<SearchConnectionDelegate>)delegate {
    self = [super init];
    if (self) {
        _loadedData = nil;
        _delegate = delegate;
    }
    return self;
}

//+ (SearchConnection *)sharedInstance
//{
//    static SearchConnection * _sharedInstance = nil;
//    static dispatch_once_t oncePredicate;
//    dispatch_once(&oncePredicate, ^{
//        _sharedInstance = [[SearchConnection alloc] init];
//    });
//    return _sharedInstance;
//}
- (void)connect:(NSString*)url {
    
    url = [url stringByAddingPercentEscapesUsingEncoding:
     NSUTF8StringEncoding];
    
    url = [NSString stringWithFormat:@"%@%@",kBaseURL,url];
    
    NSLog(@"%@",url);
    
    [dataTask cancel];
    _loadedData = [[NSDictionary alloc] init];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    dataTask = [session
        dataTaskWithURL:[NSURL URLWithString:url]
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error!=nil) {
                [_delegate searchConnectionErrorWithText:[error localizedDescription]];
                return;
            }
            if (data==nil) {
                [_delegate searchConnectionErrorWithText:@"No data"];
                return;
            }
            NSLog(@"_loadedData: %@",_loadedData);
            _loadedData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate searchConnectionFinishedLoadingData:_loadedData];
            });
            
        }];
    [dataTask resume];
}


@end
