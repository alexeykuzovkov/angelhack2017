//
//  FieldViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "FieldViewController.h"

@interface FieldViewController ()

@end

@implementation FieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [_gameFieldView drawRect:self.view.frame];

    [_gameFieldView setBackgroundColor:[UIColor clearColor]];
    [_gameFieldView setDelegate:self];
}

-(void)viewDidAppear:(BOOL)animated {
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prizeClicked:(NSDictionary*)prize {
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:[prize objectForKey:@"review"]
                                 message:[NSString stringWithFormat:@"Необходимо набрать: %d",[[prize objectForKey:@"points"] intValue]]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ок"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                         }];
    
    [view addAction:ok];
    [self presentViewController:view animated:YES completion:nil];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:_gameFieldView]) {
        [_gameFieldView makeParralax];
    }
}
@end
