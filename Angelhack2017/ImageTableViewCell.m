//
//  ImageTableViewCell.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
