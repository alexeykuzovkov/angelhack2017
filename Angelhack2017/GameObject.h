//
//  GameObject.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameObject : NSObject

@property (nonatomic) int milestonePointsCount;
@property (nonatomic) int planetIndex;
@property (nonatomic, strong) NSString *name;

-(id)initWithMilestonePointsCount:(int)pointsCount;

+(GameObject*)objectWithPoints:(int)pointsCount name:(NSString*)name planet:(int)planetIndex;
@end
