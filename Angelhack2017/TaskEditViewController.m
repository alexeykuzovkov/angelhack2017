//
//  TaskEditViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "TaskEditViewController.h"

@interface TaskEditViewController ()

@end

@implementation TaskEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_taskData!=nil) {
        [_nameField setText:[_taskData objectForKey:@"text"]];
        [_pointsControl setSelectedSegmentIndex:([[_taskData objectForKey:@"points"] intValue]-1)/2];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)save:(id)sender {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    if (!_taskData) {
        NSString *url = [NSString stringWithFormat:@"/tasks/create/%@/%@/%d", _gameId, [_nameField text],((int)[_pointsControl selectedSegmentIndex])*2+1];
        [_connection connect:url];
    }
    else {
        NSString *url = [NSString stringWithFormat:@"/tasks/edit_text/%@/%@/%d", [_taskData objectForKey:@"id"], [_nameField text],((int)[_pointsControl selectedSegmentIndex])*2+1];
        [_connection connect:url];
    }
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
