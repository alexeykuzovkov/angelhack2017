//
//  GameFieldView.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "GameFieldView.h"
#import "GameObject.h"

#define kPathPointsMultiplier 5
#define kPointSize 2
#define kPathFrequencyMultiplier 0.05
#define kHeightMultiplier 1.5
#define kPlanetHeightDivider 3
#define kTopMargin 80
#define kBottomMargin 100


@interface GameFieldView()
{
    NSArray *gameObjects;
    UIImageView *player;
    UIImageView *starsBG;
    
    NSTimer *moveTimer;
}
@end




#define getx(y,width) (sin(y*kPathFrequencyMultiplier-M_PI/4)*(width/2-40)+width/2)
//#define angleTo(x1,y1,x2,y2) (acos((x1*x2+y1*y2)/(sqrt(x1*x1+y1*y1)*(sqrt(x2*x2+y2*y2)))))
#define angleTo(x1,y1,x2,y2) (atan((y1-y2)/(x1-x2)))

#define degreesToRadians(degrees) (M_PI * degrees / 180.0)
#define radiansToDegrees(rad) ((180.0 * rad) / M_PI)

@implementation GameFieldView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect {
    gameObjects = @[
//        [GameObject objectWithPoints:50 name:@"Поход в кино" planet:1],
//        [GameObject objectWithPoints:20 name:@"Поход в зоопарк" planet:2],
//        [GameObject objectWithPoints:20 name:@"Поход в зоопарк" planet:4],
//        [GameObject objectWithPoints:40 name:@"Поход в зоопарк" planet:5],
//        [GameObject objectWithPoints:20 name:@"Поход в зоопарк" planet:6],
//        [GameObject objectWithPoints:30 name:@"Поход в зоопарк" planet:7],
    ];
    _points = 0;
    _position = 0;
    
    [self drawGame];
}

-(void)setPrizes:(NSArray *)prizes {
    _prizes = prizes;
    NSMutableArray *objs = [[NSMutableArray alloc] init];
    for (NSDictionary *prize in _prizes) {
        [objs addObject:[GameObject objectWithPoints:[[prize objectForKey:@"points"] intValue]
                                                name:[prize objectForKey:@"review"]
                                              planet:[[prize objectForKey:@"model_id"] intValue]]];
    }
    
    gameObjects = [NSArray arrayWithArray:objs];
    [self drawGame];
}
-(void)setPoints:(int)points {
    _points = points;
    
    NSLog(@"%d",_points);
    
}

-(void)drawGame {
    for (id subview in [self subviews]) {
        [(UIView*)subview removeFromSuperview];
    }
    int pointCounter = 0;
    
    starsBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stars_fon.png"]];
    [starsBG setContentMode:UIViewContentModeScaleAspectFill];
    [starsBG setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:starsBG];
    
    NSLog(@"%f",self.frame.size.width);
    
//    float prev_x = 0;
    for (GameObject *o in gameObjects) {
        for (int i = 0; i < o.milestonePointsCount*kPathPointsMultiplier; i++) {
            float y = pointCounter*kHeightMultiplier;
            float x = getx(y, self.frame.size.width);
            
            
            if (i<o.milestonePointsCount) {
//                float y = pointCounter*8;
//                float x  = floor(sinf(y/20-3.14/4)*self.frame.size.width/4+self.frame.size.width/2);
//                y = y+1/((x-prev_x)*20);
//                prev_x = x;
                
                UIView *pointView = [[UIView alloc]
                                     initWithFrame:
                                     CGRectMake(
                                                x,
                                                floor(y)+kTopMargin,
                                                kPointSize,
                                                kPointSize)];
                [pointView setBackgroundColor:[UIColor whiteColor]];
                [self addSubview:pointView];
                
                pointCounter++;
            }
            else {
                UIImage *planetImage = [UIImage imageNamed:[NSString stringWithFormat:@"planet_%d.png",o.planetIndex]];
                UIImageView *iv = [[UIImageView alloc] initWithImage:planetImage];
                [iv setFrame:CGRectMake(x-(planetImage.size.width/kPlanetHeightDivider)/2,
                                        y+kTopMargin-(planetImage.size.height/kPlanetHeightDivider)/2, planetImage.size.width/kPlanetHeightDivider, planetImage.size.height/kPlanetHeightDivider)];
                [self addSubview:iv];
                
                UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
                [b setFrame:iv.frame];
                [b setTag:[gameObjects indexOfObject:o]+100];
                [b addTarget:self action:@selector(showPrizeInfo:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:b];
            }
        }
    }
    
    
    
    player = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ship.png"]];
    [player setFrame:CGRectMake(0,0,
                                player.image.size.width,
                                player.image.size.height)];
    [self _positionPlayer];
    [self addSubview:player];
    
    [self setContentSize:CGSizeMake(self.frame.size.width,  self.frame.size.height/2+pointCounter*kHeightMultiplier+kTopMargin+kBottomMargin)];
}

-(void)showPrizeInfo:(id)sender {
    [self.delegate prizeClicked:[_prizes objectAtIndex:(int)[(UIView*)sender tag]-100]];
}

-(void)_positionPlayer {
    player.transform = CGAffineTransformIdentity;
    
    float y = _position*kHeightMultiplier;
    float x = getx(y, self.frame.size.width);
    
    float y2 = (_position+1)*kHeightMultiplier;
    float x2 = getx(y2, self.frame.size.width);
    
    
    if (x<=x2) {
        player.transform = CGAffineTransformMakeRotation(angleTo(x, y, x2, y2));
        
    }
    else {
        player.transform = CGAffineTransformMakeRotation(angleTo(x, y, x2, y2)+M_PI);
    }
    player.center = CGPointMake(x, floor(y)+kTopMargin);
    
    
    
}
-(void)beginPlayerMovement {
    NSLog(@"beginPlayerMovement %d",_points);
    if (moveTimer) {
        [moveTimer invalidate];
    }
    moveTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(movePlayer)
                                               userInfo:nil
                                                repeats:TRUE];
}
-(void)movePlayer {
    _position+=1;
    NSLog(@"pos:%d poi:%d",_position,_points);
    if (_position>=_points) {
        if (moveTimer) {
            [moveTimer invalidate];
        }
        _position = _points;
    }
    [self _positionPlayer];
    
}


-(void)makeParralax {
    [starsBG setFrame:CGRectMake(0, self.contentOffset.y*0.5, starsBG.frame.size.width, starsBG.frame.size.height)];
}

@end
