//
//  ArchiveViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchiveViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
