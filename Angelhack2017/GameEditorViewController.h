//
//  GameEditorViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"

@interface GameEditorViewController : UIViewController <SearchConnectionDelegate>
@property (nonatomic, strong) IBOutlet UIImageView *qrImageView;

@property (nonatomic, strong) IBOutlet UIButton *prizesButton;
@property (nonatomic, strong) IBOutlet UIButton *tasksButton;

@property (nonatomic, strong) NSString *gameId;

-(IBAction)cancel:(id)sender;

@end
