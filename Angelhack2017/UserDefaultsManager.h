//
//  UserDefaultsManager.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsManager : NSObject
+(void)save:(id)value forKey:(NSString*)key;
+(void)remove:(NSString*)key;
+(id)load:(NSString*)key;
@end
