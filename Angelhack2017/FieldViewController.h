//
//  FieldViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameFieldView.h"

@interface FieldViewController : UIViewController <UIScrollViewDelegate, GameFieldDelegate>
@property (strong, nonatomic) IBOutlet GameFieldView *gameFieldView;

@end
