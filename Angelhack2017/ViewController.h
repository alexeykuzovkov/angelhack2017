//
//  ViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTBBarcodeScanner.h"

@interface ViewController : UIViewController
{
    MTBBarcodeScanner *_scanner;
    UIView *_scannerPreview;
}
-(IBAction)login:(id)sender;
-(IBAction)join:(id)sender;

@end

