//
//  GameObject.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject

-(id)initWithMilestonePointsCount:(int)pointsCount
{
    self = [super init];
    if (self) {
        _milestonePointsCount = pointsCount;
    }
    return self;
}
+(GameObject*)objectWithPoints:(int)pointsCount name:(NSString*)name planet:(int)planetIndex {
    GameObject *obj = [[GameObject alloc] initWithMilestonePointsCount:pointsCount];
    [obj setName:name];
    [obj setPlanetIndex:(planetIndex>8)?8:(planetIndex<1?1:planetIndex)];
    return obj;
}

@end
