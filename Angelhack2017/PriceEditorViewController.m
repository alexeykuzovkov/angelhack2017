//
//  PriceEditorViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "PriceEditorViewController.h"

@interface PriceEditorViewController ()

@end

@implementation PriceEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (_prizeData) {
        NSLog(@"%@",_prizeData);
        [_priceName setText:[_prizeData objectForKey:@"review"]];
        [_daysField setText:[NSString stringWithFormat:@"%d",[[_prizeData objectForKey:@"days"] intValue]]];
        [_collection selectItemAtIndexPath:[NSIndexPath indexPathForRow:[[_prizeData objectForKey:@"model_id"] intValue]-1 inSection:0] animated:false scrollPosition:UICollectionViewScrollPositionTop];
    }
    else {
        [_collection selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:false scrollPosition:UICollectionViewScrollPositionTop];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)save:(id)sender {
    int selectedIndex = (int)[(NSIndexPath*)[[_collection indexPathsForSelectedItems] objectAtIndex:0] row];
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    
    if (!_prizeData) {
        NSString *url = [NSString stringWithFormat:@"/prizes/create/%@/%@/%d/%@", _gameId, [_priceName text],selectedIndex+1,[_daysField text]];
        [_connection connect:url];
    }
    else {
        NSString *url = [NSString stringWithFormat:@"/prizes/change_review/%@/%@/%d/%@", [_prizeData objectForKey:@"id"], [_priceName text],selectedIndex+1,[_daysField text]];
        [_connection connect:url];
    }
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 8;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cell.selected) {
        cell.backgroundColor = [UIColor grayColor]; // highlight selection
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor]; // Default color
    }
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"planet_%d.png",(int)(indexPath.row+1)]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
    datasetCell.backgroundColor = [UIColor grayColor]; // highlight selection
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
    datasetCell.backgroundColor = [UIColor clearColor]; // Default color
}
@end
