//
//  LoginViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "LoginViewController.h"
#import "UserDefaultsManager.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login:(id)sender {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/users/auth/%@/%@", [_loginField text], [_passwordField text]]];

}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"loaded data %@",loadedData);
    if([loadedData objectForKey:@"token"]) {
        [UserDefaultsManager save:[loadedData objectForKey:@"token"] forKey:@"admin_token"];
        [self performSegueWithIdentifier:@"games" sender:nil];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
