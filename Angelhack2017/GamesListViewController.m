//
//  GamesListViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "GamesListViewController.h"
#import "TaskTableViewCell.h"
#import "GameEditorViewController.h"
#import "UserDefaultsManager.h"
@interface GamesListViewController ()
{
    NSArray *gamesList;
}
@end

@implementation GamesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    gamesList = @[];

    [self reloadGames];
}

-(void)reloadGames {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/get_by_token/%@", [UserDefaultsManager load:@"admin_token"]]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"loaded data %@",loadedData);
    if([loadedData objectForKey:@"data"]) {
        gamesList = (NSArray*)[loadedData objectForKey:@"data"];
        [_tableView reloadData];
    }
    else {
        [self reloadGames];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

-(IBAction)createGame:(id)sender {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/create/1/0/0/token/%@", [UserDefaultsManager load:@"admin_token"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TaskCell";
    TaskTableViewCell *cell = (TaskTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"TaskTableViewCell"
                                                              owner:self
                                                            options:nil];
        cell = [arrayCellXib objectAtIndex:0];
    }
    
    
    [cell.taskNameLabel setText:@"Игра"];
    [cell.taskNumber setText:[NSString stringWithFormat:@"%d",(int)indexPath.row+1]];
    
    if(indexPath.row>2) {
        [cell setIsDone:true];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return gamesList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@",[[gamesList objectAtIndex:indexPath.row] objectForKey:@"id"]);
    [self performSegueWithIdentifier:@"game_editor" sender:[[gamesList objectAtIndex:indexPath.row] objectForKey:@"id"]];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"game_editor"]) {
        UINavigationController *nav = [segue destinationViewController];
        GameEditorViewController *e = (GameEditorViewController*)[[nav viewControllers] objectAtIndex:0];
        e.gameId = (NSString*)sender;
    }
}
@end
