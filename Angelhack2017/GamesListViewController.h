//
//  GamesListViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"

@interface GamesListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SearchConnectionDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tableView;
-(IBAction)createGame:(id)sender;
@end
