//
//  GameFieldView.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GameFieldView;
@protocol GameFieldDelegate <NSObject>
-(void)prizeClicked:(NSDictionary*)prize;
@end


@interface GameFieldView : UIScrollView
@property (nonatomic, weak) id <GameFieldDelegate> delegate;

@property (nonatomic) int position;
@property (nonatomic) int points;
@property (nonatomic, strong) NSArray *prizes;
-(void)beginPlayerMovement;
-(void)makeParralax;
@end
