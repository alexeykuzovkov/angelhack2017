//
//  TaskTableViewCell.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *taskNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *taskNumber;
@property (strong, nonatomic) IBOutlet UIImageView *taskNumberBG;

@property (nonatomic) BOOL isDone;
@end
