//
//  ViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ViewController.h"
#import "UserDefaultsManager.h"
#import "GameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initScanner];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login:(id)sender {
    if (![UserDefaultsManager load:@"admin_token"]) {
        [self performSegueWithIdentifier:@"login" sender:nil];
    }
    else {
        [self performSegueWithIdentifier:@"games" sender:nil];
    }
}

-(IBAction)join:(id)sender {
    if (![UserDefaultsManager load:@"user_token"]) {
//        [self performSegueWithIdentifier:@"login" sender:nil];
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [_scannerPreview setHidden:false];
                NSError *error = nil;
                [_scanner startScanningWithResultBlock:^(NSArray *codes) {
                    AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                    
                    
                    [_scanner stopScanning];
                    [_scannerPreview setHidden:true];
                    
                    NSLog(@"%@",code.stringValue);
                    
                    [self performSegueWithIdentifier:@"show_game" sender:code.stringValue];
                } error:&error];
                
            } else {
                // The user denied access to the camera
                [self performSegueWithIdentifier:@"show_game" sender:@"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMyIiwiaWF0IjoxNDk4OTc0MTQyfQ.gd0qeMWkL4Bjq2CsyYckoHzHljjDQQAA5GGgDuL5jZ4"];
            }
        }];

    }
    else {
//        [UserDefaultsManager save: forKey:<#(NSString *)#>]
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"show_game"]) {
        GameViewController *vc = (GameViewController*)[segue destinationViewController];
        vc.game_token = (NSString*)sender;
    }
}

-(void)initScanner {
    _scannerPreview = [[UIView alloc] initWithFrame:self.view.frame];
    [_scannerPreview setHidden:true];
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-80, self.view.frame.size.width, 80)];
    [cancelButton setTitle:@"Отменить" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelScanner) forControlEvents:UIControlEventTouchUpInside];
    [_scannerPreview addSubview:cancelButton];
    
    [self.view addSubview:_scannerPreview];
    _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreview];
}
-(void)cancelScanner {
    [_scanner stopScanning];
    [_scannerPreview setHidden:true];
}

@end
