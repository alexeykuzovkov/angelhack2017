//
//  GameViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"
#import "FieldViewController.h"
#import "TasksTableViewController.h"

@interface GameViewController : UIViewController <SearchConnectionDelegate, TaskViewDelegate>
@property (nonatomic, strong) NSString *game_token;
@end
