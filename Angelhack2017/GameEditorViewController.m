//
//  GameEditorViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "GameEditorViewController.h"
#import "PrizesListViewController.h"
#import "TasksListViewController.h"

@interface GameEditorViewController ()
{
    NSDictionary *data;
}
@end

@implementation GameEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:false animated:false];
    
}

-(void)viewDidAppear:(BOOL)animated {
    SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"/games/get_info/%@", _gameId]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"loaded data %@",loadedData);
    if([loadedData objectForKey:@"data"]) {
        data = [loadedData objectForKey:@"data"];
        NSLog(@"data %@",data);
        [_tasksButton setTitle:[NSString stringWithFormat:@"%d",(int)[(NSArray*)[data objectForKey:@"tasks"] count]] forState:UIControlStateNormal];
        [_prizesButton setTitle:[NSString stringWithFormat:@"%d",(int)[(NSArray*)[data objectForKey:@"prizes"] count]] forState:UIControlStateNormal];
        [self.qrImageView setImage:[UIImage imageWithCIImage:[self createQRForString:[data objectForKey:@"token"]]]];
    }
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    
    CIImage *input = qrFilter.outputImage;
    CGAffineTransform transform = CGAffineTransformMakeScale(5.0f, 5.0f); // Scale by 5 times along both dimensions
    CIImage *output = [input imageByApplyingTransform: transform];
    
    return output;
}

-(IBAction)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"prizes"]) {
        PrizesListViewController *pz = (PrizesListViewController*)[segue destinationViewController];
        [pz setGameId:_gameId];
        [pz setPrizes:(NSArray*)[data objectForKey:@"prizes"]];
    }
    if ([segue.identifier isEqualToString:@"tasks"]) {
        TasksListViewController *pz = (TasksListViewController*)[segue destinationViewController];
        [pz setGameId:_gameId];
        [pz setTasks:(NSArray*)[data objectForKey:@"tasks"]];
    }
}


@end
