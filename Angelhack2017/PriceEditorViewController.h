//
//  PriceEditorViewController.h
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 02.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchConnection.h"

@interface PriceEditorViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, SearchConnectionDelegate>

@property (strong, nonatomic) IBOutlet UITextField *priceName;
@property (strong, nonatomic) IBOutlet UITextField *daysField;

@property (nonatomic, strong) NSDictionary *prizeData;
@property (nonatomic, strong) NSString *gameId;

@property (nonatomic, strong) IBOutlet UICollectionView *collection;

-(IBAction)save:(id)sender;
@end
