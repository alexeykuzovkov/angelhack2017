//
//  TasksTableViewController.m
//  Angelhack2017
//
//  Created by Alex Kuzovkov on 01.07.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "TasksTableViewController.h"
#import "TaskTableViewCell.h"

@interface TasksTableViewController ()

@end

@implementation TasksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TaskCell";
    TaskTableViewCell *cell = (TaskTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *arrayCellXib = [[NSBundle mainBundle] loadNibNamed:@"TaskTableViewCell"
                                                              owner:self
                                                            options:nil];
        cell = [arrayCellXib objectAtIndex:0];
    }
    
    NSDictionary *t = [_tasks objectAtIndex:indexPath.row];
    [cell.taskNameLabel setText:[t objectForKey:@"text"]];
    [cell.taskNumber setText:[NSString stringWithFormat:@"%d",(int)indexPath.row+1]];
    
    if([[t objectForKey:@"finished"] intValue]==1) {
        [cell setIsDone:true];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

-(void)setTasks:(NSArray *)tasks {
    _tasks = tasks;
    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *task = [_tasks objectAtIndex:indexPath.row];
    if ([[task objectForKey:@"finished"] intValue]==1) {
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        return;
    }
    
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Выполнить задачу?"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Отменить"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [tableView deselectRowAtIndexPath:indexPath animated:true];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Выполнить"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [tableView deselectRowAtIndexPath:indexPath animated:true];
                             SearchConnection *_connection = [[SearchConnection alloc] initWithDelegate:self];
                             [_connection connect:[NSString stringWithFormat:@"/tasks/finish/%d", [[task objectForKey:@"id"] intValue]]];
                         }];
    
    [view addAction:ok];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}
-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    [_delegate taskViewDidChange];
}
-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
